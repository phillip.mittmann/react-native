import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
//import { View, Text } 'react-native';
import reducers from './reducers';
import firebase from 'firebase';
import LoginForm from './components/LoginForm';

class App extends Component { 
	componentWillMount() {
  		const config = {
    			apiKey: 'AIzaSyCfXTzUX-tkJCpSHHU96f9o9kBWaoSCpD8',
    			authDomain: 'manager-9a81d.firebaseapp.com',
    			databaseURL: 'https://manager-9a81d.firebaseio.com',
    			projectId: 'manager-9a81d',
    			storageBucket: 'manager-9a81d.appspot.com',
    			messagingSenderId: '731901510127'
  		};

  		firebase.initializeApp(config);
	}

	render() {
		return (
			<Provider store={createStore(reducers)}>
				<LoginForm />
			</Provider>
		);
	}
}

export default App;